import api from './api'

export function getEvents (startDate, endDate) {
  return api.post('/events', { params: { startDate: startDate, endDate: endDate } })
}
